
## Interview Test for Application Support Analist

1. Clone the MASTER branch of this repo https://bitbucket.org/dkirkhope/interview/
2. Create a branch named your name. For example I would call the branch `davidkirkhope`
3. For each part. Create a sub directory called answers.
4. Go though each part.
5. When you have finished push all your changes in the branch to the remote server.


## Git Help

How to Clone
```
$ git clone https://dkirkhope@bitbucket.org/dkirkhope/interview.git
```

How to create a branch
```
$ git checkout -b [name_of_your_new_branch]
```

How to push the branch
```
$ git push origin [name_of_your_new_branch]
```