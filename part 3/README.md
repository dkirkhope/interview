
## Part Three

1. Copy and run seed.sh on the server
2. sync the files from /data/folder1 to /data/folder2. Making sure to capture the output of the command.
3. Create a simple report to prove each file is the same in each folder1 and folder2. Use commandline tools and output the report as a CSV.
4. Delete files from /data/folder1 and sync the changes to /data/folder2. Create another report to show which files were deleted.

## Backup

1. Create a new directory called backup in the root of the file system.
2. Using tar create a backup of /data/folder1 and place it in the backup directory. 
