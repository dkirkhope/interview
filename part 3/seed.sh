#!/bin/bash

mkdir -p /data/folder1 /data/folder2

for n in {1..10}; do
    RAN=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 4 | head -n 1)
    mkdir -p /data/folder1/$RAN${n}
    for i in {1..30}; do
        export RAN${n}=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 4 | head -n 1)
        dd if=/dev/urandom of=/data/folder1/$RAN${n}/file$( printf %03d "$i" ).bin bs=1 count=$(( RANDOM + 1024 ))
    done
done

