## Part Four

Create an OS user account for the following new members of Ops. 

- Charlie
- Dennis
- Deandra
- Frank

Ensure they

1. have the ability to run any commands with `sudo`. 
2. have strong passwords that they will need to change on first login
3. are a member of the `operations` group

Next, create a shared directory `/var/ops_share/` for them to share files with each other. Make sure the permissions allow them all to read/write in the directory. Make sure that only they have access.