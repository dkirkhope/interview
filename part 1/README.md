## Part One

You've been given longrunningscript.sh script to run. The script only needs to be run once as part of a process.

The script will take about 4 hours to execute and should not be interrupted once its started.

What can you do to make sure the script keeps running even if your connection is cut?