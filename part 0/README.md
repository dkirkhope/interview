
# The website is down

 A customer raises the following ticket.

> Hi Support, 
> 
> We're getting a white page when trying to navigate to  http://inter.arkivum.net/
> 
> Please can someone take a look?
>  
>  Regards,
>  Frank

## Task

 - Respond to the customer. 
 - SSH on to the server and work out what the
   issue it (the IP address of the server is the same as the FQDN given
   by the customer).
 - Try and solve the issue
  
**If the site is back online**
  
 - Write a respond for internal use explaining what the issue was and how you fixed it. 
 - Write a respond for the customer. 

**If you were unable to resolve the issue**

- Write a response for internal use explaining what you are seeing and what you may have tried.
- Write a responce for the customer

